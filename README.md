## 将pytorch model转成caffe2 mdoel.

### 修改与说明
基于[Pytorch教程](https://github.com/onnx/tutorials/blob/master/tutorials/PytorchCaffe2MobileSqueezeNet.ipynb), 我做了一些修改和整合.

- 在utils.py中

```
import caffe2.python.onnx.backend  --> import onnx_caffe2.backend
from caffe2.python.onnx.backend import Caffe2Backend as c2  --> from onnx_caffe2.backend import Caffe2Backend as c2
```
原因是在onnx2caffe2函数中执行
```
init_net, predict_net = c2.onnx_graph_to_caffe2_net(model.graph)", it returns a error
```
报错：
```
AttributeError: 'GraphProto' object has no attribute 'graph'
```

- 在SqueezeNet.py的squeezenet1_1函数中添加了pre-trained model保存路径

- 设置了后续生成的onnx, caffe2的保存路径


### 关于环境配置

注： 本机是ubuntu 16.04(64位), python默认版本是2.7

#### 安装caffe2
- [源码安装caffe2](https://caffe2.ai/docs/getting-started.html?platform=ubuntu&configuration=compile)

- 注意： caffe2目前只支持python2.7. 官方解释是Python is core to run Caffe2. We currently require Python2.7.
        Ubuntu 14.04 and greater have Python built in by default, and that can be used to run Caffe2. 
        To check your version: python --version

  因此, cmake时务必确保编译的python版本是2.7, 也可以cmake时指定python解释器

- cmake参数设置
```
cmake -D USE_MPI=OFF ..
```
注. USE_MPI=OFF是为了防止编译错误(如下)（[解决方法来源](https://github.com/caffe2/caffe2/issues/2144)
）

```
recipe for target 'caffe2/CMakeFiles/mpi_test.dir/all' failed
make[1]: *** [caffe2/CMakeFiles/mpi_test.dir/all] Error 2
```

#### 安装onnx_caffe2, pip安装


- 注1. 不要用conda安装, conda安装会出现问题
- 注2. 虽然说onnx_caffe2已经合并到caffe2, 但onnx_caffe2依然可用


#### 安装[onnx](https://github.com/onnx/onnx), pip安装

```
sudo pip install git+git://github.com/onnx/onnx.git@master
```

- 注1. 如果是用pip install onnx, 之后import caffe2.python.onnx.backend会报错
```
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "/home/hrs/installations/caffe2/build/caffe2/python/onnx/backend.py", line 42, in <module>
    import onnx.optimizer
ImportError: No module named optimizer

```
[解决方法](https://github.com/onnx/onnx/issues/582): 
```
sudo pip uninstall onnx
sudo pip install git+git://github.com/onnx/onnx.git@master 
```
- 注2. 虽然onnx_caffe2已经合并到caffe2, 但是

##### 附录 - Error 1

```
CRITICAL:root:Cannot load caffe2.python. Error: No module named caffe2_pybind11_state
```
[解决方法](https://github.com/caffe2/caffe2/issues/963)

##### 附录 - Error 2

```
CRITICAL:root:Cannot load caffe2.python. Error: libcaffe2.so: cannot open shared object file: No such file or directory

```
解决方法：
```
sudo find / -name libcaffe2.so
# 得到两个同名文件
# /home/hrs/installs/caffe2/build/lib/libcaffe2.so
# /usr/local/lib/libcaffe2.so
# 应该是之前安装了caffe2, 重新编译后python导致找不到这个文件
sudo cp /home/hrs/installs/caffe2/build/lib/libcaffe2.so /usr/local/lib/libcaffe2.so
```
